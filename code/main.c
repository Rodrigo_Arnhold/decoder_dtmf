#include <stdio.h>
#include <stdlib.h>
#include <math.h>






double goertzelFilter(char* samples, double freq, int N, int k) {
   
    double z0 = 0, z1 = 0, z2 = 0;
    double coeff, sine, real, imag;
    int i;
    double omega = (2*M_PI*k) / N;
    double cosine = cos(omega);
	coeff = 2*cosine;
    sine = sin(omega);
    for (i=0; i<N; i++) {
        z0 = coeff * z1 - z2 + samples[i] ;
        z2 = z1;
        z1 = z0;
    }
    
    real = (z1 - z2 * cosine) / N;
  	imag = (z2 * sine) / N;

  	return sqrt(real*real + imag*imag);
    
}


const int Fi[] = {697, 770, 852,941,1209,1336,1477,1633};
double K[8];
double s1[8];
double s2[8];
double Power[8];

void teste(char* sample, int N){
	
	int i,j;
	double z0 = 0, A = 0, B = 0, Sm = 0;
	
	for(i=0; i< 8; i++){		
		z0 = (N * Fi[i] / 8000);
		z0 = (2 * M_PI * z0) / N;
		K[i] = 2 * cos(z0);
		printf("\nk[%d]: %.3f", i, K[i]);		
	}
	
	
	for(j=0;j<N; j++){
		for(i = 0; i < 8; i++){				
			z0 = K[i] * s1[i] - s2[i] + sample[j];
			s2[i] = s1[i];
			s1[i] = z0;	
/*
			z0 = K[i] * s1[i];
			z0 = z0 - s2[i];
			z0 = z0 + sample[j];
			s2[i] = s1[i];
			s1[i] = z0;
			*/
		}		
	}
	
	for(i = 0; i < 8; i++){		
		A = s1[i] * s1[i];
		B = s2[i] * s2[i];
		Sm = A + B;
		z0 = A * B * K[i];
		Power[i] = (Sm - z0) * -1;
	}

	
	
}


int main(int argc, char *argv[]) {
	
	
	FILE *ptr_file;
	char buf[1000];
	double d[7][10];
	unsigned int i = 0, j;

	ptr_file = fopen("C:\\Users\\Rodrigo\\Desktop\\decoder_dtmf\\dtmf_txt\\6online.txt","r");
	while( (buf[i++]=fgetc(ptr_file))!= EOF){
		if(i > 998){
			break;
		}
	}
	fclose(ptr_file);
	
	printf("\n%s\n size: %d",&buf[0], strlen(buf));

    
    memset(s1, 0 , 8);
    memset(s2, 0 , 8);
    
    teste(&buf[60], 205);
    
	j = 0;
	i = 1;
	while(j < 8){
		printf("\nPower[%d]: %.3f", Fi[j] ,Power[j] );
		j++;
	}
	
	j = 0;
	i = 1;
	while(j < 4){
		if(Power[j] > Power[j+i]){
			i = j;
		}
		j++;	
	}
	printf("\nPower[%d]: %.3f", Fi[i] ,Power[i] );
	
	j = 4;
	i = 1;
	while(j < 8){
		if(Power[j] > Power[j+i]){
			i = j;
		}
		j++;	
	}
	printf("\nPower[%d]: %.3f", Fi[i] ,Power[i] );
	
		
    
    
    
 	/*   
	j = 0;
	while(j++ < 7)
		d[j][0] = 0;

		
	j = i = 0;
	char n = 95	;		
    while(i < 10){

		d[j + 0][i]  = goertzelFilter(&buf[i + n], 1209.0, n,16);
		d[j + 1][i]  = goertzelFilter(&buf[i + n], 1336.0, n,18);
		d[j + 2][i]  = goertzelFilter(&buf[i + n], 1477.0, n,19);
		d[j + 3][i]  = goertzelFilter(&buf[i + n], 770.0,  n,10);	
		d[j + 4][i]  = goertzelFilter(&buf[i + n], 852.0,  n,11);
		d[j + 5][i]  = goertzelFilter(&buf[i + n], 941.0,  n,12);	
		d[j + 6][i]  = goertzelFilter(&buf[i + n], 697.0,  n,9);
		
		i++; 
	}
	i = 0;
	while(i < 7){
		j = 0;
		while(j < 10){
			printf("\n j[%d]: %.3f", i,d[i][j]);
			j++;	
		}
		i++;
	}
*/
	
	return 0;
}
